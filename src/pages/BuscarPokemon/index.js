import React, { useEffect, useRef, useState } from "react";
import axios from "axios";
import api from "../../services/api";
// import Cards from "../../components/Cards/Index";
import Options from "../../components/Inputs/SelectOptions/Index";
export default function BuscarPokemon() {
  const [urlPokemon, setUrlPokemon] = useState();

  useEffect(() => {
    const array = [];
    var promise1 = new Promise((resolve1, reject1) => {
      api
        .get("pokemon/?limit=150")
        .then((res) => {
          const results = res.data.results;
          array.push(...results);

          setUrlPokemon(array);
          resolve1(array);
        })
        .catch((err) => {
          console.log("erro", err);
        });
    });

    Promise.all([promise1])
      .then((values) => {
        console.log("ok!");
      })
      .catch((err) => {
        console.log("Erro!", err);
      });
  }, []);

  return (

    <div className="flex flex-col  items-center gap-3 py-16">
      
        <Options list={urlPokemon} label="Pokemon" name="pokemon" />
    </div>
  );
}
