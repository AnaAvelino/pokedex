import React from "react";
import Psyduck from "../../assets/psyduck404.jpg";

export default function Page404() {
  return (
    <div className="flex flex-col p-10 py-28">
      <div className=" flex  justify-center ">
        <p className="font-semibold text-4xl">?</p>
        <img
          className=" w-2/12 rounded-full bg-yellow-500 border-2 "
          src={Psyduck}
        />
        <p className="font-semibold text-4xl">?</p>
      </div>
      <div className="h-2"></div>
      <div className=" flex justify-center font-semibold text-2xl">
        <div> Psyduck Confused, page not found </div>
      </div>
    </div>
  );
}
