import React from "react";
import Pokedex from "../../assets/pokedex-removebg-preview.png";
import { Link } from "react-router-dom";

export default function Header({ title }) {
  return (
    <div className="flex  text-gray-800 font-semibold text-2xl p-4 capitalize bg-white border-b-2">
      <Link to="/">
        <div className="flex flex-row">
          <img className="w-8" src={Pokedex} />
          <div className="flex items-center ml-1">{title}</div>
        </div>
      </Link>
    </div>
  );
}
