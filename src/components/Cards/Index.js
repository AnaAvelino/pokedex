import React, { useEffect, useRef, useState } from "react";
import axios from "axios";
import Pokebola from "../../assets/pokebola-removebg-preview.png";
import Grass from "@mui/icons-material/Grass";
import Science from "@mui/icons-material/Science";
import LocalFireDepartment from "@mui/icons-material/LocalFireDepartment";
import Water from "@mui/icons-material/Water";
import Bug from "@mui/icons-material/BugReport";
import PanoramaFishEye from "@mui/icons-material/PanoramaFishEye";
import AcUnit from "@mui/icons-material/AcUnit";


export default function Cards({ pokemon, type, abilities, moves }) {
  const pokemonDefault = { img: Pokebola, name: "Procurando um Pokémon?" };

  const [dataPokemon, setDataPokemon] = useState([pokemonDefault]);

  useEffect(() => {
    if (pokemon.length != 0) {
      setDataPokemon(pokemon);
    }
  }, [pokemon]);

  function customizeType(customType) {
    switch (customType) {
      case "grass":
        return (
          <div className="bg-green-800 p-1 pr-2  rounded-md font-semibold flex items-center">
            <Grass className="pr-1" />
            {customType}
          </div>
        );
      case "poison":
        return (
          <div className="bg-purple-800 p-1 pr-2  rounded-md font-semibold flex items-center">
            <Science className="pr-1" />
            {customType}
          </div>
        );
      case "fire":
        return (
          <div className="bg-red-800 p-1 pr-2 rounded-md font-semibold flex items-center">
            <LocalFireDepartment className="pr-1" />
            {customType}
          </div>
        );
      case "bug":
        return (
          <div className="bg-green-600 p-1 pr-2 rounded-md font-semibold flex items-center">
            <Bug className="pr-1" />
            {customType}
          </div>
        );
      case "water":
        return (
          <div className="bg-blue-800 p-1 pr-2 rounded-md font-semibold flex items-center">
            <Water className="pr-1" />
            {customType}
          </div>
        );
        case "ice":
          return (
            <div className="bg-blue-900 p-1 pr-2 rounded-md font-semibold flex items-center">
              <AcUnit className="pr-1" />
              {customType}
            </div>
          );
      default:
        return (
          <div className="bg-gray-400 p-1 pr-2 rounded-md font-semibold items-center flex">
            <PanoramaFishEye className="pr-1" />
            <p>{customType}</p>
          </div>
        );
    }
  }

  function TypeMap(array) {
    return array.map((value, index) => {
      return customizeType(value.type.name);
    });
  }
  return (
    <>
      <div className="grid grid-cols-1 border-2 rounded-md ">
        {dataPokemon.map((value, index) => {
          return (
            <div className=" bg-slate-100  text-white ">
              <div className="flex justify-center  rounded-t-lg p-4 rou">
                <img className="h-44" src={value.img} />
              </div>
              <div className="p-4 bg-gray-700 rounded-t-2xl  rounded-b-md shadow-lg">
                <div className="h-8"></div>
                <div className="font-mono font-semibold text-lg capitalize">
                  {value.name}
                </div>
                <div className="h-4"></div>
                {/* <div className="font-mono  text-sm">Descrição</div> */}
                <div className="h-4"></div>
                <div className="font-mono  text-sm">
                  <div className="flex flex-row gap-3">{TypeMap(type)}</div>
                </div>
                <div className="h-5"></div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}
