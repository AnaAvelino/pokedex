import React, { useEffect, useRef, useState } from "react";
import api from "../../../services/api";
import axios from "axios";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import Search from "@mui/icons-material/Search";
import Card from "../../Cards/Index";
import Loader from "../../Load";

export default function SelectOptions({ options, list, label, name }) {
  const textField = useRef("");
  const [pokemon, setPokemon] = useState([]);
  const [type, setType] = useState([]);
  const [loader, setLoader] = useState(false);

  const handlerSubmit = (e) => {
    e.preventDefault();
    setLoader(true);
    const pokemonSelected = textField.current.value;
    api
      .get(`pokemon/${pokemonSelected}`)
      .then((res) => {
        const array_pokemon = [];
        const array_types = [];
        const array_moves = [];
        const dados = [];
        const array_abilities = [];

        var result = res.data;
        array_pokemon.push(result);
        array_pokemon.map((value, index) => {
          const img = value.sprites.other.dream_world.front_default;
          const name = value.name;
          const type = value.types;
          const abilities = value.abilities;
          const moves = value.moves;
          dados.push({ img: img, name: name, type });
          array_types.push(...type);
          array_abilities.push(abilities);
          array_moves.push(moves);

        });
        setTimeout(() => {
          setLoader(false);
        }, 2000);
        setPokemon(dados);
        setType(array_types);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="flex flex-col">
      <form className="" onSubmit={handlerSubmit}>
        <div className="flex flex-row">
          <Autocomplete
            id="pokemon"
            className="flex-1 "
            options={list}
            getOptionLabel={(option) => option.name.toString()}
            selectOnFocus
            renderInput={(params) => (
              <TextField
                {...params}
                label={label}
                variant="outlined"
                name={name}
                inputRef={textField}
                value={textField}
                required
              />
            )}
          />
          <div className="h-5 w-5"></div>
          <button
            type="submit"
            className="bg-blue-600 text-white w-12 rounded-lg"
          >
            <Search />
          </button>
        </div>
      </form>
      {loader === true ? (
      <Loader Loading={loader} />
      ) : (
        <>
          <div className="w-80 p-2"></div>
          <Card pokemon={pokemon} type={type} />
        </>
      )}
    </div>
  );
}
