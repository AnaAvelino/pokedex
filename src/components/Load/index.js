import { React, useState, useEffect } from "react";
import PikachuGit from "../../assets/pikachu.gif";

export default function Loader({ Loading }) {
  const [load, setLoad] = useState(true);

  return (
    <>
      {load === true ? (
        <div className=" flex w-72 border-b-2  justify-center my-20">
          <img className="w-44" src={PikachuGit} />
        </div>
      ) : (
        ""
      )}
    </>
  );
}
