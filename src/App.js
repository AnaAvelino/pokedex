import "./App.css";
import BuscarPokemon from "./pages/BuscarPokemon";
import Page404 from "./pages/Page404";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Header from "./components/Header";

function App() {
  return (
    <div className="App bg-gray-50 h-screen">
      <Router>
        <Header title="Pokedex Web" />
        <Routes>
          <Route exact path="/" element={<BuscarPokemon />} />
          <Route path="*" element={<Page404 />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
