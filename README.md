### aplicação desenvolvida com React.js

### Para iniciar o projeto use o comando
#### 1º `npm install`
#### 2º `npm start`

#### Descrição
#### `Com o pokedex é possivel buscar diversos pokemons, basta selecionar qualquer pokemon da lista e é possivel ver sua imagemn nome e tipo,pois consumimos uma Api que nos traz informações sobre o pokemon selecionado, em breve tambem será possivel ver uma lista com o seus principais ataques. `
 
 **Projeto tem como objetivo por em pratica habilidades adquiridas em aula, os quais são:**
 
 1. Consumo de Api de terceiros, usando AXIOS.
 2. Uso de Hooks.
 3. Uso do pacote react router dom.
 4. Construção de componentes resposivos, usando (Flex).
 5. Tratativa de erros.


